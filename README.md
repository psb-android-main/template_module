## Интеграция в существующий модуль


#### 1. Добавляем переменные окружения

для windows
```
SETX ACCESS_KEY "**********"
SETX SECRET_KEY "**********"
SETX URL "s3://psb.android.s3.amazonaws.com"
```

для linux
```
EXPORT ACCESS_KEY="**********"
EXPORT SECRET_KEY="**********"
EXPORT URL="s3://psb.android.s3.amazonaws.com"
```

для gitlab (ci/cd) нужно в settings > ci/cd > variables:
```
ACCESS_KEY="**********"
SECRET_KEY="**********"
URL="s3://psb.android.s3.amazonaws.com"
```

#### 2. Прописываем откуда следует скачивать модули (в нашем случае это s3 хранилище) - build.gradle

```
allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url System.getenv('URL')
            credentials(AwsCredentials) {
                accessKey(System.getenv('ACCESS_KEY'))
                secretKey(System.getenv('SECRET_KEY'))
            }
        }
    }
}
```

#### 3.  Теперь нужно изменить build.gradle(app) для того чтобы наш модуль можно было пушить в s3

```
plugins {
    id 'com.android.library'
    id 'kotlin-android'
    id 'maven-publish'
}

// подключаем нужные плагины

apply plugin: 'com.android.library'
apply plugin: 'maven-publish'

// update these next lines to fit your submodule
group = 'com.credit' // название группы
version = '1.1' // версия нашего модуля которая будет в s3

// Add sources as an artifact
task sourceJar(type: Jar) {
    from android.sourceSets.main.java.srcDirs
    classifier "source"
}

// ....

// Loop over all variants
android.libraryVariants.all { variant ->
    variant.outputs.all { output ->
        // This creates a publication for each variant
        publishing.publications.create(variant.name, MavenPublication) {
            // The sources artifact from earlier
            artifact sourceJar

            // Variant dependent artifact, e.g. release, debug
            artifact source: output.outputFile, classifier: output.name

            // Go through all the dependencies for each variant and add them to the POM
            // file as dependencies
            pom.withXml {
                def dependencies = asNode().appendNode('dependencies')

                // Filter out anything that's not an external dependency. You shouldn't
                // be publishing artifacts that depend on local (e.g. project) dependencies,
                // but who knows...
                configurations.getByName(variant.name + "CompileClasspath").allDependencies
                        .findAll { it instanceof ExternalDependency }
                        .each {
                            def dependency = dependencies.appendNode('dependency')

                            dependency.appendNode('groupId', it.group)
                            dependency.appendNode('artifactId', it.name)
                            dependency.appendNode('version', it.version)
                        }
            }
        }
    }
}

// Ensure that the publish task depends on assembly
tasks.all { task ->
    if (task instanceof AbstractPublishToMaven) {
        task.dependsOn assemble
    }
}

// task для проверки что env переменные работают правильно
task printEnvs() {
    doLast {
        println(System.getenv('ACCESS_KEY'))
        println(System.getenv('SECRET_KEY'))
        println(System.getenv('URL'))
    }
}


// Configure the destination repository with
// S3 URL and access credentials
publishing {
    repositories {
        maven {
            url System.getenv('URL')
            credentials(AwsCredentials) {
                accessKey(System.getenv('ACCESS_KEY'))
                secretKey(System.getenv('SECRET_KEY'))
            }
        }
    }
}
```

#### 4. Теперь мы можем выкатывать модуль в s3

`gradlew app:publish`


#### 5. Также мы можем подключить и использовать существующие модули из s3 

```
dependencies {
    //...........
    api group: 'com.stylelibrary', name: 'app', version: '1.4', ext: 'aar', classifier: 'debug'
    api group: 'com.eventbus', name: 'app', version: '1.5', ext: 'jar'
}
```
